# Manual Test Management Demo

## Purpose
This demo project shows how GitLab features, such as projects, issues, boards, labels, and CI can be used to help manage manual testing efforts while we continue to work on first-class test management features.